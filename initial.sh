#!/bin/bash
# Setup Script V. 1.0
# git clone git@bitbucket.org:lbrower/initial.git


declare -rx VERSION="1.0"
declare -rx REL="DEV"

declare -arx LOCNETS=( 10.0.0.0/24 192.168.1.0/24 )

color() {
. /etc/init.d/functions
red='\e[0;31m' ; RED='\e[1;31m' ; blue='\e[0;34m' ; BLUE='\e[1;34m'
cyan='\e[0;36m' ; CYAN='\e[1;36m' ; NC='\e[0m' ; Black='\e[0;30m'
Dark_Gray='\e[1;30m' ; Blue='\e[0;34m' ; Light_Blue='\e[1;34m' ; Green='\e[0;32m'
Light_Green='\e[1;32m' ; Cyan='\e[0;36m' ; Light_Cyan='\e[1;36m' ; Red='\e[0;31m'
Light_Red='\e[1;31m' ; Purple='\e[0;35m' ; Light_Purple='\e[1;35m' ; Brown='\e[0;33m'
Yellow='\e[1;33m' ; Light_Gray='\e[0;37m'; White='\e[1;37m'
return 0
}

print() { color; printf "\n${Cyan}[+] $* ${NC}\n"; return 0; }
att() { color; printf "\n${red}****[ ATTENTION ]****: $* ${NC}\n"; return 0; }
sepp() { color; printf "\n${Light_Gray}---------- $* ----------${NC}\n\n"; return 0; }
note() { color; printf "\n\n${Purple}***NOTE: $*\n\n${NC}"; return 0; }


Say_Ver() {
clear
color
printf "${BLUE}"
printf "\n\n%40s %60s\n" "Setup Script" "Version: ${VERSION}-${REL}"
printf "%40s %60s\n\n" "Distribution: $(uname -s)" "Arch: $(uname -m)"
# uname -s outputs Linux and works on Solaris. uname -o doesnt work on solaris.
printf "${NC}"
sleep 3
return 0
}

Add_SSH_Banner() {
color
print "Creating SSH banner"
if [ -f /etc/banner ]
then
    att "/etc/banner exists. Backing up prior to writing."
    cp -a /etc/banner{,.Backup-$(date +%Y-m-d)}
fi

touch /etc/banner
chmod 644 /etc/banner
cat << BANNER_EOF > /etc/banner

You are connected to $(uname -n)


This is a private computer system and is the property of SolSys6.Net. It is for authorized users only. Unauthorized users are prohibited. Users (authorized or unauthorized) have no explicit or implicit expectation of privacy. Any or all uses of this system may be subject to one or more of the following actions: interception, monitoring, recording, auditing, inspection and disclosing to security personnel and law enforcement personnel, as well as authorized officials of other agencies, both domestic and foreign. By using this system, the user consents to these actions. Unauthorized or improper use of this system may result in administrative disciplinary action and civil and criminal penalties. By accessing this system you indicate your awareness of and consent to these terms and conditions of use. Discontinue access immediately if you do not agree to the conditions stated in this notice.


BANNER_EOF
print "Banner File has been created"
return 0
}

Update_SSH_Config() {
color
print "Updating SSH Config"
print "Backing up sshd_config"
cp -a /etc/ssh/sshd_config{,.Backup-$(date +%F)}

cat << EOF_SSHDCONF > /etc/ssh/sshd_config
Port 22
#AddressFamily any
ListenAddress 0.0.0.0
#ListenAddress ::
Protocol 2
HostKey /etc/ssh/ssh_host_key
HostKey /etc/ssh/ssh_host_rsa_key
KeyRegenerationInterval 1h
ServerKeyBits 2048
SyslogFacility AUTHPRIV
#LogLevel INFO
LoginGraceTime 30
PermitRootLogin without-password
StrictModes yes
MaxAuthTries 3
MaxSessions 4
PubkeyAuthentication yes
AuthorizedKeysFile     .ssh/authorized_keys
RhostsRSAAuthentication no
HostbasedAuthentication no
IgnoreUserKnownHosts no
IgnoreRhosts yes
PasswordAuthentication yes
PermitEmptyPasswords no
ChallengeResponseAuthentication yes
KerberosAuthentication no
GSSAPIAuthentication no
UsePAM yes
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv XMODIFIERS
AllowAgentForwarding no
AllowTcpForwarding no
GatewayPorts no
X11Forwarding no
PrintMotd yes
PrintLastLog yes
TCPKeepAlive yes
UsePrivilegeSeparation yes
ClientAliveInterval 0
ClientAliveCountMax 3
UseDNS no
PermitTunnel no
#ChrootDirectory none
Banner /etc/banner

# override default of no subsystems
Subsystem       sftp    /usr/libexec/openssh/sftp-server

EOF_SSHDCONF
print "sshd_config recreated"
print "Restarting SSH"
/etc/init.d/sshd restart >>/dev/null 2>&1 
if [ "$?" -eq "0" ]
then
	success
else
	failure
fi
return 0
}

Install_Firewall() {
color
print "Installing APF"
cd /usr/local/src
wget -q http://www.rfxn.com/downloads/apf-current.tar.gz
wget -q http://www.rfxn.com/downloads/bfd-current.tar.gz
tar zxf apf-current.tar.gz
tar zxf bfd-current.tar.gz
cd /usr/local/src/apf*/
/bin/sh install.sh >>/dev/null 2>&1 

cat /root/initial/Confs/etc/apf/conf.apf > /etc/apf/conf.apf

/usr/local/sbin/apf -r >>/dev/null 2>&1 
print "Installing BFD"

cd /usr/local/src/bfd*/
/bin/sh install.sh >>/dev/null 2>&1

cat /root/initial/Confs/usr/local/bfd/conf.bfd > /usr/local/bfd/conf.bfd


for NET in ${LOCNETS[@]}
do
	/usr/local/sbin/apf -a ${NET}
done
print "Firewall Installed"
return 0
}

RFXN() {
color
print "Installing Some Stuff From RFXN"
# ---- Begin Download and Install Applications ----
cd /usr/local/src/

APPS=(les-current.tar.gz maldetect-current.tar.gz lsm-current.tar.gz prm-current.tar.gz spri-current.tar.gz)

for APP in ${APPS[@]} 
do
	wget -q http://www.rfxn.com/downloads/${APP}
	tar zxf ${APP}
done
for DIR in les lsm maldetect prm spri
do
	sepp "Installing ${DIR}"
	cd ${DIR}-*
	/bin/sh install.sh >> /dev/null 2>&1
	cd /usr/local/src/
done
# ---- End Download and Install Applications ---- 

# Run LES 
sepp "Running LES"
/usr/local/sbin/les -ea >> /dev/null 2>&1 

# Run LSM
print "Adding lsm cronjob..."
printf "/usr/local/sbin/lsm -c\n" > /etc/cron.hourly/lsm

cat /root/initial/Confs/usr/local/maldetect/conf.maldet >/usr/local/maldetect/conf.maldet


/usr/local/sbin/maldet -d >> /dev/null 2>&1
/usr/local/sbin/maldet -u >> /dev/null 2>&1
/usr/local/sbin/maldet -m /home,/root,/tmp,/dev/shm
printf "\n@reboot /usr/local/sbin/maldet -m /home,/root,/tmp,/dev/shm\n" >> /etc/crontab

cat /root/initial/Confs/usr/local/prm/conf.prm > /usr/local/prm/conf.prm

/usr/local/sbin/spri -v 

return 0
}

CCGU() {
color
print "Adding get-users group"
/usr/sbin/groupadd get-users >>/dev/null 2>&1
/usr/sbin/groupadd deva >>/dev/null 2>&1
print "chowning / chattring some common utilities"
DAPPS=(/usr/bin/GET /usr/bin/wget /usr/bin/curl /usr/bin/lwp-* /usr/bin/links /usr/bin/elinks)
for APP in ${DAPPS[@]}
do
	chown root:get-users ${APP} 2>/dev/null
	/bin/chmod 750 ${APP}  2>/dev/null
	/usr/bin/chattr +ia ${APP}  2>/dev/null
done
return 0
}

UPDATESTUFF() {
color
cat << EOF_UPDATE_CRON >/etc/cron.daily/Daily_Update
MAILTO="noc@maxqe.com"
DAPPS=(/usr/bin/GET /usr/bin/wget /usr/bin/curl /usr/bin/lwp-* /usr/bin/links /usr/bin/elinks)
for APP in ${DAPPS[@]}
do
        /usr/bin/chattr -ia ${APP}
done
/usr/local/sbin/les -da
/usr/bin/yum -y upgrade
/usr/local/sbin/les -ea
for APP in ${DAPPS[@]}
do
        /usr/bin/chattr +ia ${APP}
done
EOF_UPDATE_CRON

return 0
}

ST() {
color
# secure tmp.
return 0
}

NC() {
color
print "Securing nobody crontab"
chattr -ia /var/spool/cron/nobody  2>/dev/null
>/var/spool/cron/nobody  2>/dev/null
chattr +ia /var/spool/cron/nobody  2>/dev/null
echo nobody >> /etc/cron.deny
print "crontab secured"
return 0
}

SERVICES() {
color
print "Adding / Removing / Enabling Some Services"
ASERVICES=(audit ntp ypbind nss iptables rpcbind postfix jwhois)
RSERVICES=( sssd sendmail xinetd telnet-server telnet krb5-workstation rsh-server rsh tftp-server sendmail vsftpd isdn4k-utils irda-utils talk cups tomcat6 certmonger )

for SERVICE in ${ASERVICES[@]}
do
	yum -y install ${SERVICE} 2>/dev/null 

done

for SERVICE in auditd ypbind sssd iptables rpcbind postfix sshd
do
/sbin/chkconfig ${SERVICE} on >>/dev/null  2>&1
/sbin/service  ${SERVICE} start >>/dev/null 2>&1
done

for SERVICE in ${RSERVICES[@]}
do
	yum -y erase ${SERVICE}  2>/dev/null  

done
return 0
}


INSTALL_AIDE() {
color
print "Installing AIDE"
yum -y install aide 2>/dev/null
print "Initalizing AIDE...."
/usr/sbin/aide --init >>/dev/null 2>&1 
 
mv -v /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz 
print "Adding AIDE cronjob..."
printf "\n@daily /usr/sbin/aide --check\n">/etc/cron.daily/aide

return 0 
}

SetupNTP() {
color
print "Enabling NTP"
/sbin/chkconfig ntpd on
/sbin/service ntpd start 
return 0
}

AddSSHKey() {
color
print "Adding SSH Keys to root"
/bin/chmod 700 /root
if [ -d /root/.ssh ]
then
	chown root:root /root/.ssh
	/bin/chmod 700 /root/.ssh
else
	mkdir -p /root/.ssh
        chown root:root /root/.ssh
        /bin/chmod 700 /root/.ssh
fi
if [ -f /root/.ssh/authorized_keys ]
then
	att "/root/.ssh/authorized_keys EXISTS. Moving to /root and recreating file."
	mv /root/.ssh/authorized_keys /root/authorized_keys--original--$(date +%F)
	touch /root/.ssh/authorized_keys
	chown root:root /root/.ssh/authorized_keys
	chmod 644 /root/.ssh/authorized_keys
fi

cat << DEFAULT_KEYS_EOF >/root/.ssh/authorized_keys 

ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA2ypcLuLYu/BkRujnjuRCl+wiPRzu4+Tiinqgc8KLNX/f3BMABJIUIhEfpmwL5KEj4c7DGIZirVhM6bUhbrZKnEuVV2UQvc0dOSG31T9BUDewppLVmY9JSW7HXM1vKeIc9KXGu1vu/8XnnYmp0QunIWQQ6MT9oOAstK55pZ9FxaXyfyxKQY6LxeebNhFvxXi9jgJdS/zI1Bk+4FfAiRI2r5tuy1SWr5T7lwTA+HB5VUymZt/lLhZ6TM8HscPFK/MXqySgAT86Ty5lKyVKMsM3FAtdoYjOzrWSvNn1K2S+4JF0j+DFjeOGSWYZ9PG9hYlZjNBNdlrg87HPeAx5K6t2FQ== Atlas /root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAtq7Mojb45aHhcrbM/PHwWu2OL9JbcYtvjv/21UBk02Kzenjifpw/xFLjDRCVmOucfmvvlTHz3ivyMB+iM37r1bKX3lLsYqb3eCX1LiTU4hrxrDpkMxjXMNINZKhyxJqSAZgxfz3d902kuHNGfRuW0svHfqNuHChtogVXq4LSrthZ5Fju9sb5RNALP9rSeqlyW1A8Qfb2iFtIaL+K9ZCoJUhCZpaK6pfDeXYm4Hfbre+6g5iAtTQpXSXjH81hzqZ+bn49dslWDMOPsQtBshDSCiYRd/sTDV3ybDkqkIHV2AUbYRrAEpktSCrjfgi/4YbMOlqtrsxGZ+n/5uf3aoFc7w== lbrower@atlas.solsys6.net
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAgEAsPzmJKGglcK2fr1jBYn5gYoOMAwRCmM9TerIIjm6YhHFDMifqWbOcd9TuC9+Rvhv0/+rPY2F9OkoYmDP5gFOKTjwGANDgsGBwQXRwb2USdetJe0nDkUavjkwj9i8gaXPamd1uD4zmzyg8Ghn2d6LWgvhU2S+aX0jUr7OqwDL/Le4+V0JMFOHfHJekKsTh7DTkYBeJnhsPmi6FqS24gjEQvSCNJOXSvyq44TckGuNb+epgabEMEnDs/HYKIdWoWKLny4ZItYx1Su5ayfdYVov9B/A0H0O53OVSxn7ZmeMirlxh+OQHhR+ulBDBiIdJhin3F6kTnNI8as5HcSAbNeIpkze3wsMENRqZGIzBBfy3Ta5yHCDY28sxOwrlyFz2ENcri3+Kub3qYL9vj8jeq/S6/UyVea7I7CIkqHw29uMtdsht6Gx6l07ZAt6MOabysZ+1rF5P7zMqa5JGBDItAbdvVjUf7CYi0KAzKkrG650hsqou+jQ8OElPUCYeKjn1A0zR++DtkUcjVTo6HXLspmlB1U46rxMZPBMgi0q1CdIc8larzo9GEstaOJH4wKego+qUBo/QUfGjlmFhCL6F449xNJHDORhViEXClHqaljDZsQi+4dkM+ES7unhIsn7WI40erWR15HqQwi3hAwFpi1YB/R4Mu+lZPxNwBnubTGPn3M= lbrower@atlas.solsys6.net

DEFAULT_KEYS_EOF
print "SSH Keys Added"
return 0
}

SYSUP() {
color
print "Performing System Update"
sleep 1
yum -y upgrade 2>/dev/null | dot
ldconfig
return 0
}

SetupNISandLDAP() {

# add stuff here to enable/configure NIS and LDAP

return 0 
}



dot() {
color
while read LINE
do
printf "${CYAN}";printf ".";sleep 0.02;printf "${Purple}";printf ".";sleep 0.02;printf "${Green}";printf ".";sleep 0.02;printf "${Light_Purple}";printf ".";sleep 0.02
printf "${RED}";printf ".";sleep 0.02;printf ".";sleep 0.02;printf ".";sleep 0.02;printf ".";sleep 0.02
printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02
printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02
printf " ";sleep 0.02;printf " ";sleep 0.02;printf " ";sleep 0.02;printf " ";sleep 0.02
printf " ";sleep 0.02;printf " ";sleep 0.02;printf " ";sleep 0.02;printf " ";sleep 0.02
printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02
printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02
printf "${BLUE}";printf ".";sleep 0.02;printf "${White}";printf ".";sleep 0.02;printf "${Green}";printf ".";sleep 0.02;printf "${Light_Purple}";printf ".";sleep 0.02
printf "${NC}";printf ".";sleep 0.02;printf ".";sleep 0.02;printf ".";sleep 0.02;printf ".";sleep 0.02
printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02
printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02
printf " ";sleep 0.02;printf " ";sleep 0.02;printf " ";sleep 0.02;printf " ";sleep 0.02
printf " ";sleep 0.02;printf " ";sleep 0.02;printf " ";sleep 0.02;printf " ";sleep 0.02
printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02
printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02;printf "\b";sleep 0.02

done
return 0
}



Say_Ver
Add_SSH_Banner
AddSSHKey
#SetupNTP
Update_SSH_Config
Install_Firewall
SERVICES
SetupNTP
SYSUP
RFXN
NC
CCGU
INSTALL_AIDE

#ST
#UPDATESTUFF
